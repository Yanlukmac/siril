# Build properties
option('relocatable-bundle', type: 'combo', value: 'platform-default',
                             description: 'build with resources considered bundled under the same prefix',
                             choices: [ 'yes', 'no', 'platform-default' ])
option('openmp', type : 'boolean', value : true,
                 description: 'build with OpenMP support')

